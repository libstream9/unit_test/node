#include <stream9/node/node.hpp>

#include <boost/test/unit_test.hpp>

#include <sstream>

#include <stream9/optional.hpp>

namespace testing {

namespace st9 { using namespace stream9; }

using st9::node;

struct foo { int a; };

struct bar : foo { bar(int v) : foo { v } {} };

BOOST_AUTO_TEST_SUITE(node_)

    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(case1_)
        {
            node<int> i { 100 };

            BOOST_TEST(i == 100);
        }

        BOOST_AUTO_TEST_CASE(from_derived_type_)
        {
            node<bar> i1 { 100 };
            node<foo> i2 { std::move(i1) };

            BOOST_TEST(i2->a == 100);
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_SUITE(equality_)

        BOOST_AUTO_TEST_CASE(same_node_)
        {
            node<int> i1 { 100 };
            node<int> i2 { 200 };

            BOOST_TEST(i1 != i2);
        }

        BOOST_AUTO_TEST_CASE(different_node_)
        {
            node<int> i1 { 100 };
            node<long> i2 { 100 };

            BOOST_TEST(i1 == i2);
        }

        BOOST_AUTO_TEST_CASE(different_type_)
        {
            node<int> i1 { 100 };
            double i2 { 100 };

            BOOST_TEST(i1 == i2);
        }

    BOOST_AUTO_TEST_SUITE_END() // equality_

    BOOST_AUTO_TEST_SUITE(ordering_)

        BOOST_AUTO_TEST_CASE(same_node_)
        {
            node<int> i1 { 100 };
            node<int> i2 { 200 };

            BOOST_TEST(i1 < i2);
        }

        BOOST_AUTO_TEST_CASE(different_node_)
        {
            node<int> i1 { 200 };
            node<long> i2 { 100 };

            BOOST_TEST(i1 > i2);
        }

        BOOST_AUTO_TEST_CASE(different_type_)
        {
            node<int> i1 { 100 };
            double i2 { 200 };

            BOOST_TEST(i1 <= i2);
        }

    BOOST_AUTO_TEST_SUITE_END() // ordering_

    BOOST_AUTO_TEST_CASE(optional_)
    {
        auto o_i1 = st9::opt<node<int>>();
        BOOST_TEST(!o_i1);

        o_i1.emplace(100);
        BOOST_TEST(o_i1);
        BOOST_TEST(o_i1 == 100);
        static_assert(sizeof(o_i1) == sizeof(node<int>));
    }

    BOOST_AUTO_TEST_CASE(stream_out_)
    {
        node<int> i1 { 100 };

        std::ostringstream os;
        os << i1;

        BOOST_TEST(os.str() == "100");
    }

    BOOST_AUTO_TEST_CASE(generic_conversion_)
    {
        node<int> i1 { 100 };

        auto b = static_cast<bool>(i1);

        BOOST_TEST(b);
    }

    BOOST_AUTO_TEST_CASE(null_traits_)
    {
        using T = node<int>;

        static_assert(st9::nullable<T>);

        auto n1 = st9::make_null<T>();

        BOOST_TEST(st9::is_null(n1));

        n1 = T(1);
        BOOST_TEST(!st9::is_null(n1));

        st9::set_null(n1);
        BOOST_TEST(st9::is_null(n1));
    }

    BOOST_AUTO_TEST_CASE(swap_)
    {
        node<int> n1 { 1 };
        node<int> n2 { 2 };

        using std::swap;
        swap(n1, n2);

        BOOST_TEST(n1 == 2);
        BOOST_TEST(n2 == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // node_

} // namespace testing
