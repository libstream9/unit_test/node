#include <stream9/node/shared_node.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/optional.hpp>

namespace testing {

namespace st9 { using namespace stream9; }

using st9::shared_node;

struct foo { int a; };

struct bar : foo { bar(int v) : foo { v } {} };

BOOST_AUTO_TEST_SUITE(shared_node_)

    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(case1_)
        {
            shared_node<int> i { 100 };

            BOOST_TEST(i == 100);
        }

        BOOST_AUTO_TEST_CASE(from_derived_type_)
        {
            shared_node<bar> i1 { 100 };
            shared_node<foo> i2 { std::move(i1) };

            BOOST_TEST(i2->a == 100);
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_SUITE(equality_)

        BOOST_AUTO_TEST_CASE(same_node_)
        {
            shared_node<int> i1 { 100 };
            shared_node<int> i2 { 200 };

            BOOST_TEST(i1 != i2);
        }

        BOOST_AUTO_TEST_CASE(different_node_)
        {
            shared_node<int> i1 { 100 };
            shared_node<long> i2 { 100 };

            BOOST_TEST(i1 == i2);
        }

        BOOST_AUTO_TEST_CASE(different_type_)
        {
            shared_node<int> i1 { 100 };
            double i2 { 100 };

            BOOST_TEST(i1 == i2);
        }

    BOOST_AUTO_TEST_SUITE_END() // equality_

    BOOST_AUTO_TEST_SUITE(ordering_)

        BOOST_AUTO_TEST_CASE(same_node_)
        {
            shared_node<int> i1 { 100 };
            shared_node<int> i2 { 200 };

            BOOST_TEST(i1 < i2);
        }

        BOOST_AUTO_TEST_CASE(different_node_)
        {
            shared_node<int> i1 { 200 };
            shared_node<long> i2 { 100 };

            BOOST_TEST(i1 > i2);
        }

        BOOST_AUTO_TEST_CASE(different_type_)
        {
            shared_node<int> i1 { 100 };
            double i2 { 200 };

            BOOST_TEST(i1 <= i2);
        }

    BOOST_AUTO_TEST_SUITE_END() // ordering_

    BOOST_AUTO_TEST_CASE(optional_)
    {
        auto o_i1 = st9::opt<shared_node<int>>();
        BOOST_TEST(!o_i1);

        o_i1.emplace(100);
        BOOST_TEST(o_i1);
        BOOST_TEST(o_i1 == 100);
        //static_assert(sizeof(o_i1) == sizeof(shared_node<int>));
    }

    BOOST_AUTO_TEST_CASE(shared_from_this_)
    {
        struct baz : st9::enable_shared_from_this<baz>
        {
            int a;
        };

        shared_node<baz> n1;
        n1->a = 1;

        shared_node<baz> n2 = n1->shared_from_this();
        BOOST_TEST(n1->a == n2->a);

        shared_node<baz const> n3 = n1->shared_from_this();
        BOOST_TEST(n1->a == n3->a);
    }

BOOST_AUTO_TEST_SUITE_END() // shared_node_


} // namespace testing
